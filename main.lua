vec = require "vector"
shash = require "shash"
nuklear = require "nuklear"
require "gradient"

min = math.min
random = love.math.random

width = 0
height = 0
radius = 0

-- Configurable settings
particle_count = 500
stiffness = 60
stiffness_near = 90
rest_density = 10
interaction_radius = 35
gravity_magnitude = 100
point_size = 3
drawing_mode = 'Blobs'
skip_blobs = false
skip_points = true

-- Derived values
interaction_radius_half = 0
interaction_radius_inv = 0
interaction_radius_sq = 0
gravity_force = vec.new(0, 0)

state = {
  x = {},
  y = {},
  old_x = {},
  old_y = {},
  vx = {},
  vy = {},
  p = {},
  p_near = {},
  g = {}
}
hash_map = {}

canvas = {}
blob_shader = {}

mouse = vec.new(0, 0)
ui = {}

function love.load()
  love.window.setMode(600, 740, {
    resizable = true,
    msaa = 4,
    highdpi = true,
    minheight = 740
  })

  love.graphics.setColor(0.3, 0.8, 1)
  love.graphics.setPointSize(point_size)

  w, h = love.graphics.getDimensions()
  love.resize(w, h)

  blob_shader = love.graphics.newShader [[
    vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
    {
      vec4 pixel = Texel(texture, texture_coords);
      if (pixel.a > 0.9) {
        return pixel * color;
      } else {
        return vec4(0, 0, 0, 0);
      }
    }
  ]]

  ui = nuklear.newUI()
end

function love.resize(w, h)
  width, height = love.graphics.getDimensions()
  radius = math.min(width, height) * 0.25

  canvas = love.graphics.newCanvas()
end

function love.update(dt)
  local old_particle_count = particle_count
  if old_particle_count > #state.x then
    old_particle_count = #state.x
  end
  
  update_ui()
  hash_map = shash.new(interaction_radius * 2)

  if particle_count > old_particle_count then
    for i = old_particle_count + 1, particle_count do
      init_particle(i)
    end
  end

  for i = 1, particle_count do
    state.old_x[i] = state.x[i]
    state.old_y[i] = state.y[i]

    apply_global_forces(i, dt)

    state.x[i] = state.x[i] + state.vx[i] * dt
    state.y[i] = state.y[i] + state.vy[i] * dt

    hash_map:add(i, state.x[i], state.y[i], interaction_radius_half, interaction_radius_half)
  end

  for i = 1, particle_count do
    local neighbors = find_neighbors(i)

    update_pressure(i, neighbors)
    relax(i, neighbors, dt)
  end

  for i = 1, particle_count do
    contain(i, dt)
    calculate_velocity(i, dt)
  end
end

function update_ui()
  ui:frameBegin()
	if ui:windowBegin('Settings', width - 390, 10, 380, 310,
      'border', 'title') then

		ui:layoutRow('dynamic', 30, 3)
    ui:label('Particle count:')
    particle_count = ui:slider(100, particle_count, 10000, 100)
    ui:label(particle_count)

    ui:layoutRow('dynamic', 30, 3)
    ui:label('Stiffness:')
    stiffness = ui:slider(5, stiffness, 100, 5)
    ui:label(stiffness)

    ui:layoutRow('dynamic', 30, 3)
    ui:label('Stiffness (near):')
    stiffness_near = ui:slider(5, stiffness_near, 300, 5)
    ui:label(stiffness_near)

    ui:layoutRow('dynamic', 30, 3)
    ui:label('Rest density:')
    rest_density = ui:slider(1, rest_density, 40, 1)
    ui:label(rest_density)

    ui:layoutRow('dynamic', 30, 3)
    ui:label('Interaction radius:')
    interaction_radius = ui:slider(1, interaction_radius, 40, 1)
    ui:label(interaction_radius)

    ui:layoutRow('dynamic', 30, 3)
    ui:label('Gravity magnitude:')
    gravity_magnitude = ui:slider(10, gravity_magnitude, 200, 10)
    ui:label(gravity_magnitude)

    ui:layoutRow('dynamic', 30, 3)
    ui:label('Drawing mode:')
    drawing_mode = ui:radio('Blobs', drawing_mode)
    drawing_mode = ui:radio('Points', drawing_mode)

    ui:layoutRow('dynamic', 30, 3)
    ui:label('Point size:')
    point_size = ui:slider(2, point_size, 10, 1)
    ui:label(point_size)
	end
	ui:windowEnd()
  ui:frameEnd()
  
  interaction_radius_half = interaction_radius / 2
  interaction_radius_inv = 1 / interaction_radius
  interaction_radius_sq = interaction_radius * interaction_radius
  gravity_force = vec.new(0, gravity_magnitude)

  love.graphics.setPointSize(point_size)
end

function init_particle(i)
  -- from https://stackoverflow.com/a/5838055
  local t = 2 * math.pi * random()
  local u = random() + random()
  local r
  if (u > 1) then
    r = 2 - u
  else
    r = u
  end

  state.x[i] = r * math.cos(t) * radius
  state.y[i] = r * math.sin(t) * radius

  state.old_x[i] = state.x[i]
  state.old_y[i] = state.y[i]

  state.vx[i] = 0
  state.vy[i] = 0
  state.p[i] = 0
  state.p_near[i] = 0
  state.g[i] = 0
end

function apply_global_forces(i, dt)
  local force = vec.new(0, 0)

  force = force + gravity_force

  local from_mouse = vec.new(state.x[i], state.y[i]) - mouse
  local scalar = min(4000, 400000 / from_mouse:length_squared())
  local mouse_force = from_mouse:unit_approx() * scalar
  force = force + mouse_force

  state.vx[i] = state.vx[i] + force.x * dt
  state.vy[i] = state.vy[i] + force.y * dt
end

function find_neighbors(i)
  local neighbors = {}
  hash_map:each(i, function(j)
    local g = gradient(i, j)
    if g ~= nil then
      state.g[j] = g
      table.insert(neighbors, j)
    end
  end)

  return neighbors
end

function gradient(i, j)
  local a = vec.new(state.x[i], state.y[i])
  local b = vec.new(state.x[j], state.y[j])

  local lsq = (a - b):length_squared()
  if lsq > interaction_radius_sq then
    return nil
  end

  local distance = math.sqrt(lsq)
  return 1 - distance / interaction_radius
end

function update_pressure(i, neighbors)
  local density = 0
  local near_density = 0

  for _, j in ipairs(neighbors) do
    local g = state.g[j]
    density = density + g * g
    near_density = near_density + g * g * g
  end

  state.p[i] = stiffness * (density - rest_density)
  state.p_near[i] = stiffness_near * near_density
end

function relax(i, neighbors, dt)
  local pos = vec.new(state.x[i], state.y[i])

  for _, j in ipairs(neighbors) do
    local g = state.g[j]
    local n_pos = vec.new(state.x[j], state.y[j])

    local magnitude = state.p[i] * g + state.p_near[i] * g * g
    local direction = (n_pos - pos):unit_approx()
    local force = direction * magnitude
    local d = force * dt * dt

    state.x[i] = state.x[i] + d.x * -0.5
    state.y[i] = state.y[i] + d.y * -0.5

    state.x[j] = state.x[j] + d.x * 0.5
    state.y[j] = state.y[j] + d.y * 0.5
  end
end

function contain(i, dt)
  local pos = vec.new(state.x[i], state.y[i])

  if pos:length_squared() > radius * radius then
    local unit_pos = pos:unit()
    local new_pos = unit_pos * radius
    state.x[i] = new_pos.x
    state.y[i] = new_pos.y

    local antistick = unit_pos * (interaction_radius * dt)
    state.old_x[i] = state.old_x[i] + antistick.x
    state.old_y[i] = state.old_y[i] + antistick.y
  end
end

function calculate_velocity(i, dt)
  local pos = vec.new(state.x[i], state.y[i])
  local old = vec.new(state.old_x[i], state.old_y[i])

  local v = (pos - old) * (1 / dt)

  state.vx[i] = v.x
  state.vy[i] = v.y
end

function love.draw()
  love.graphics.setCanvas({canvas, stencil=true})

  love.graphics.clear()

  local particle_count = particle_count
  for i = 1, particle_count do
    local x, y = world_to_screen_space(state.x[i], state.y[i])

    if drawing_mode == 'Blobs' then
      love.gradient.draw(
        function()
          love.graphics.circle("fill", x, y, interaction_radius / 2)
        end,
        "radial",
        x,
        y,
        interaction_radius,
        interaction_radius,
        { 0.3, 0.8, 1 },
        { 0, 0, 0, 0 }
      )
    else
      love.graphics.points(x, y)
    end
  end

  love.graphics.setCanvas()

  love.graphics.setShader(blob_shader)
  love.graphics.draw(canvas)
  love.graphics.setShader()

  ui:draw()

  love.graphics.print("FPS: " .. love.timer.getFPS(), 10, 10)
end

function love.keypressed(key, scancode, isrepeat)
	ui:keypressed(key, scancode, isrepeat)
end

function love.keyreleased(key, scancode)
	ui:keyreleased(key, scancode)
end

function love.mousepressed(x, y, button, istouch, presses)
	ui:mousepressed(x, y, button, istouch, presses)
end

function love.mousereleased(x, y, button, istouch, presses)
	ui:mousereleased(x, y, button, istouch, presses)
end

function love.mousemoved(x, y, dx, dy, istouch)
  ui:mousemoved(x, y, dx, dy, istouch)
  
  local x, y = screen_to_world_space(x, y)
  mouse = vec.new(x, y)
end

function love.textinput(text)
	ui:textinput(text)
end

function love.wheelmoved(x, y)
	ui:wheelmoved(x, y)
end

function world_to_screen_space(x, y)
  local offset_x = width * 0.5
  local offset_y = height * 0.72

  return x + offset_x, y + offset_y
end

function screen_to_world_space(x, y)
  local offset_x = width * 0.5
  local offset_y = height * 0.72

  return x - offset_x, y - offset_y
end
