# LÖVE Fluid Simulation

![](blobs.gif)

```shell
$ brew install luajit
$ love .
```

References:
* [Particle-based Viscoelastic Fluid Simulation](http://www.ligum.umontreal.ca/Clavet-2005-PVFS/pvfs.pdf)
* [Simulating Blobs of Fluid](https://peeke.nl/simulating-blobs-of-fluid)
