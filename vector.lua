-- 2d vector math
--
-- translated from https://peeke.nl/simulating-blobs-of-fluid

local max, abs, sqrt = math.max, math.abs, math.sqrt

vec = {}
vec.__index = vec

function vec.new(x, y)
  local self = setmetatable({}, vec)
  self.x = x
  self.y = y

  return self
end

function vec.__add(a, b)
  return vec.new(
    a.x + b.x,
    a.y + b.y
  )
end

function vec.__sub(a, b)
  return vec.new(
    a.x - b.x,
    a.y - b.y
  )
end

-- vector * number: scalar product
-- vector * vector: dot product
function vec.__mul(a, b)
  if type(a) == "number" then
    return vec.new(
      b.x * a, b.y * a
    )
  elseif type(b) == "number" then
    return vec.new(
      a.x * b,
      a.y * b
    )
  end

  return vec.new(
    a.x * b.x,
    a.y * b.y
  )
end

function vec:length_squared()
  return self.x * self.x
    + self.y * self.y
end

function vec:length()
  return sqrt(self:length_squared())
end

function vec:unit()
  local length = self:length()
  if length == 0 then
    return vec.new(0, 0)
  end

  return self * (1 / length)
end

function vec:unit_approx()
  if self.x == 0 and self.y == 0 then
    return vec.new(0, 0)
  end

  local ax = abs(self.x)
  local ay = abs(self.y)

  local ratio = 1 / max(ax, ay)
  ratio = ratio * (1.29289 - (ax + ay) * ratio * 0.29289)

  return self * ratio
end

function vec:__tostring()
   return "(" .. self.x .. ", " .. self.y .. ")"
end

return vec